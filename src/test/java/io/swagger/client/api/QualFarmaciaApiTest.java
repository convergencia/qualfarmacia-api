/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.swagger.client.api;

import br.com.qualfarmacia.api.client.ApiException;
import br.com.qualfarmacia.api.client.api.AutenticacaoApi;
import br.com.qualfarmacia.api.client.api.LojaApi;
import br.com.qualfarmacia.api.client.api.ProdutoApi;
import br.com.qualfarmacia.api.client.model.AccessToken;
import br.com.qualfarmacia.api.client.model.Autenticacao;
import br.com.qualfarmacia.api.client.model.Loja;
import br.com.qualfarmacia.api.client.model.Produto;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author desenv
 */
public class QualFarmaciaApiTest {

    private final LojaApi lojaApi = new LojaApi();
    private final AutenticacaoApi autenticacaoApi = new AutenticacaoApi();
    private final ProdutoApi produtoApi = new ProdutoApi();
    private AccessToken token;

    @Test
    public void getTokenTest() throws ApiException {
        Autenticacao authentication = new Autenticacao();
        authentication.setGrantType("password");
        authentication.setUsername("Convergencia.Teste");
        authentication.setPassword("88836367-16a4-4137-a255-3917c1f3b3b3");
        token = autenticacaoApi.getToken(authentication);

        Assert.assertTrue(!(token == null || token.getToken() == null || token.getToken().isEmpty()));

        // TODO: test validations
    }

    @Test
    public void registrarLojaTest() throws ApiException {
        getTokenTest();

        lojaApi.getApiClient().setAccessToken(token.getToken());

        Loja loja = new Loja();
        loja.ID("2");
        loja.CEP("74454352");
        loja.CNPJ("72.487.467/0001-19");
        loja.CRF(null);
        loja.CRF_UF(null);
        loja.IE("123456");
        loja.UF("go");
        loja.nome("teste 2");

        List<Loja> provider = new ArrayList<Loja>();
        provider.add(loja);
        String response = lojaApi.registrarLoja(provider);

        // TODO: test validations
        Assert.assertTrue(!(response == null || response.isEmpty()));
    }

    @Test
    public void registrarProdutoTest() throws ApiException {
        getTokenTest();
        lojaApi.getApiClient().setAccessToken(token.getToken());

        String lojaId = "2";
        List<Produto> produtos = new ArrayList<Produto>();

        Produto produto = new Produto();

        produto.setApresentacao("NAO DEFINIDO");
        produto.setEan("3337871310592");
        produto.setNome("DES.VICHY ANTI-TR. 24 H AE 125");
        produto.setFabricante("NAO DEFINIDO");
        produto.setPrecobalcao(new Float(2));
        produto.setPrecominimo(new Float(2));
        produto.setQuantidadeestoque(new Float(2));

        produtos.add(produto);

        String response = produtoApi.registrarProduto(lojaId, produtos);

        // TODO: test validations
        Assert.assertTrue(!(response == null || response.isEmpty()));
    }

    @Test
    public void atualizarProdutoParcialTest() throws ApiException {

        getTokenTest();
        lojaApi.getApiClient().setAccessToken(token.getToken());

        String lojaId = "2";
        String ean = "3337871310592";
        Produto produto = new Produto();
        produto.setApresentacao("NAO DEFINIDO");
        produto.setEan("3337871310592");
        produto.setNome("DES.VICHY ANTI-TR. 24 H AE 125");
        produto.setFabricante("NAO DEFINIDO");

        Produto response = produtoApi.atualizarProdutoParcial(lojaId, ean, produto);

        // TODO: test validations
    }

}
