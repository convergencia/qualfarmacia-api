/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.qualfarmacia.api.client.model;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author desenv
 */
public class AccessToken {

    @SerializedName("access_token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
